# Are you better than Ana? 

Just messing around and learning the Discord API And also tracker.gg API.

## Requirements

- [Node.js](http://nodejs.org/)
- [Discord](https://discordapp.com/) account

## Installation Steps (if applicable)

1. Clone repo
2. Run `npm install`
3. Add Discord credentials in a `.env` file as "TOKEN=XXXXXXXXXXXX"
4. Add tracker.gg token to `.env` as "TRACKER_TOKEN=XXXXXXXXXXXX"
5. Run `node index.js`
6. Interact with your Discord bot via your web browser
