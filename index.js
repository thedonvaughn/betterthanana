require('dotenv').config();
const axios = require('axios');
const Discord = require('discord.js');
const bot = new Discord.Client();
const TOKEN = process.env.TOKEN;
const TRACKER_TOKEN = process.env.TRACKER_TOKEN

console.log(`TRACKER_TOKEN is ${TRACKER_TOKEN}`);

bot.login(TOKEN);

bot.on('ready', () => {
  console.info(`Logged in as ${bot.user.tag}!`);
});

bot.on('message', async msg => {
 
  let author;
  if ('nickname' in msg.member && msg.member.nickname !== null ) {
    author = msg.member.nickname;
  } else {
    author = msg.author.username;
  }

  if (author !== 'BetterThanAna') {
    console.log(msg);
  }

  let authorResponse;
  let authorHeadshotKills;
  console.log(`Author is ${author}`);


  if (msg.content.toLocaleLowerCase() === '!kanye' && author !== "BetterThanAna") {
    try {
      const response = await axios.get('https://api.kanye.rest');
      console.log(response.data);
      msg.channel.send(response.data.quote);
      msg.channel.send(' -- Kanye West');
      msg.channel.send('https://tenor.com/view/kanye-west-wtf-weird-dance-gif-12489524');
    } catch (err) {
      console.log(`Kanye error! ${err}`);
    }
  }

  if (msg.content.toLocaleLowerCase() === '!potato' && author !== "BetterThanAna") {
    if (author !== "BetterThanAna") {
      console.log(`${author} requested !potato`);
      try {
        authorResponse = await axios.get(`https://public-api.tracker.gg/v2/division-2/standard/profile/psn/${author}`,  { 'headers': { 'TRN-Api-Key': TRACKER_TOKEN } });
        const authorHeadshots = authorResponse.data.data.segments[0].stats.killsHeadshot;
        authorHeadshotKills = parseInt(authorHeadshots.value, 10);

        const anaResponse = await axios.get('https://public-api.tracker.gg/v2/division-2/standard/profile/psn/anastasia_p',  { 'headers': { 'TRN-Api-Key': TRACKER_TOKEN } });
        const anaHeadshots = anaResponse.data.data.segments[0].stats.killsHeadshot;
        const anaHeadshotKills = parseInt(anaHeadshots.value, 10);
    
        if (anaHeadshotKills > authorHeadshotKills) {
          const difference = anaHeadshotKills - authorHeadshotKills;
    
          msg.reply(` Ana has ${anaHeadshotKills} headshot kills and is in the ${anaHeadshots.percentile}% percentile.`);
          msg.reply(` You only have ${authorHeadshotKills} headshot kills. Ana has ${difference} more than you.  Git gud scrub`);
          msg.channel.send('*Stats from tracker.gg*');
          msg.channel.send('https://tenor.com/view/gitgud-thepruld-git-gud-dark-gif-8126281');
    
        } else {
          const difference = authorHeadshotKills - anaHeadshotKills;
    
          msg.reply(` Ana has ${anaHeadshotKills} headshot kills and is in the ${anaHeadshots.percentile}% percentile.`);
          msg.reply(` but you have ${authorHeadshotKills} headshot kills. You have ${difference} more than Ana!`);
          msg.channel.send('*Stats from tracker.gg*');
          msg.channel.send('https://tenor.com/view/badass-watch-out-oh-okay-gif-4368018')
        }
      } catch (err) {
        console.log(`Error! ${err}`);
        msg.reply('Bro - set your discord name to match your PSN.  Fucker');
      }
    }
  } else if (msg.content.toLowerCase().startsWith('!betterthan') && author !== "BetterThanAna") {
    if (author !== "BetterThanAna") {
      console.log(`${author} requested !potato`);
      try {
        authorResponse = await axios.get(`https://public-api.tracker.gg/v2/division-2/standard/profile/psn/${author}`,  { 'headers': { 'TRN-Api-Key': TRACKER_TOKEN } });
        const authorHeadshots = authorResponse.data.data.segments[0].stats.killsHeadshot;
        authorHeadshotKills = parseInt(authorHeadshots.value, 10);
        console.log(`${author} requested !betterThan`);
        const mentions = msg.content.split(' ');
        const taggedUser = mentions[1];
        if (mentions.length === 2) {
          if (taggedUser.toLocaleLowerCase() === 'jaysonmercs') {
            msg.reply(` No one is better than ${taggedUser}.  Have you no shame?!`);
            msg.channel.send('https://tenor.com/view/walking-dead-get-the-fuck-outta-here-get-the-fuck-out-gtfo-throw-gif-4225473');
            msg.channel.send('*Stats from no where.  JaysonMercs is insecure with his mediocrity*');
          } else {
            try {
            console.log(`taggedUser is ${taggedUser}`);
            const taggedResponse = await axios.get(`https://public-api.tracker.gg/v2/division-2/standard/profile/psn/${taggedUser}`,  { 'headers': { 'TRN-Api-Key': TRACKER_TOKEN } });
            const taggedHeadshots = taggedResponse.data.data.segments[0].stats.killsHeadshot;
            const taggedHeadshotKills = parseInt(taggedHeadshots.value, 10);

            if (taggedHeadshotKills > authorHeadshotKills) {
              const difference = taggedHeadshotKills - authorHeadshotKills;
    
              msg.reply(` ${taggedUser} has ${taggedHeadshotKills} headshot kills and is in the ${taggedHeadshots.percentile}% percentile.`);
              msg.reply(` You only have ${authorHeadshotKills} headshot kills. ${taggedUser} has ${difference} more than you.  Git gud scrub`);
              msg.channel.send('*Stats from tracker.gg*');
              msg.channel.send('https://tenor.com/view/gitgud-thepruld-git-gud-dark-gif-8126281');
            } else {
              const difference = authorHeadshotKills - taggedHeadshotKills;

              msg.reply(` ${taggedUser} has ${taggedHeadshotKills} headshot kills and is in the ${taggedHeadshots.percentile}% percentile.`);
              msg.reply(` but you have ${authorHeadshotKills} headshot kills. You have ${difference} more than ${taggedUser}!`);
              msg.channel.send('*Stats from tracker.gg*');
              msg.channel.send('https://tenor.com/view/badass-watch-out-oh-okay-gif-4368018')
            }
          } catch (err) {
            msg.reply(`${taggedUser} is not a valid PSN!  Idiot...`);
          }
        }
      } else {
        msg.reply('Must provide a PSN name to compare yourself with!   Moron...');
      }
    } catch (err) {
      console.log(`Error! ${err}`);
      msg.reply('Bro - set your discord name to match your PSN.  Fucker');
    }
   }
  } else if (msg.content === '!wtf') {
    msg.channel.send('BetterThanAna - greatest discord bot to ever live!');
    msg.channel.send('!potato - Are you better than Ana?!');
    msg.channel.send('!betterThan [PSN Name] - Are you better than [PSN Name]?');
    msg.channel.send('!kanye - Quotes from the man himself');
    msg.channel.send('``` Stats from tracker.gg ```');
    msg.channel.send('``` Written by JaysonMercs - Not affiliated with Ana (But she probably has better aim than you) and inspired by Yeezus```');
  }
});
